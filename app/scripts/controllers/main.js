'use strict';

/**
 * @ngdoc function
 * @name testprojappApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the testprojappApp
 */
angular.module('testprojappApp')
  .controller('MainCtrl', function ($scope, $http) {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];

    var onUserComplete = function(response){
      $scope.places = response.data.Result;
    };

    $http.get('http://pagesjaunesresto-api-dev.azurewebsites.net/Api/Catalog/SearchPlaceByPlaceNameOrZipCode?q=Paris&pageNumber=0&pageSize=25&apiKey=EE9C8401-1436-4957-A464-2212718B6FBF')
        .then(onUserComplete);
  

  });
