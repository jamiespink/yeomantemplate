'use strict';

/**
 * @ngdoc function
 * @name testprojappApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the testprojappApp
 */
angular.module('testprojappApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
